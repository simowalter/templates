# templates

## Example Use of Kaniko template:

In your .gitlab-ci.yml file, include the template:

```bash
include:
  - project: 'simowalter/templates'
    ref: main
    file: '/templates/.gitlab-ci-kaniko.yml'
```

Then set the following variables for the templates
* `IMAGE_NAME`: name of the image (e.g. 'my_image')
* `IMAGE_TAG`: the tag of the image (e.g. 'latest', '1.0')
* `GROUP`: the group of the project on which you are using this template
* `PROJECT`: the name of the project on which you are using this template
* `URL_REGISTRY`: URL of a private registry on which you want to push the image
* `INSECURE_REGISTRY`: set it to true of the registry is insecure (without ssl/tls) and false otherwise

```bash
variables:              
  IMAGE_NAME: my-image
  IMAGE_TAG: latest
  GROUP: pro
  PROJECT: test
  URL_REGISTRY: "144.24.25.23:5000"
  INSECURE_REGISTRY: "true"
```

You also need to set a Gitlab CI variables: 
* `REGISTRY_USERNAME`
* `REGISTRY_PASSWORD`
* `CERT_REGISTRY`: [only for registry secured with SSL/TLS] the certifate of the registry ( should be set as a file !) 
